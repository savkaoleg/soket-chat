var express = require('express')
  , app = express()
  , http = require('http')
  , server = http.createServer(app)
  , routes = require('./routes')
  , socket = require('./routes/socket.js')
  , io = require('socket.io').listen(server);

var config = require('./config')(app, express);

app.get('/', routes.index);
app.get('/partials/:name', routes.partials);

app.get('*', routes.index);

io.sockets.on('connection', socket);

var port = 5000;

server.listen(port);